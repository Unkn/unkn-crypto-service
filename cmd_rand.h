/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CMD_RAND_H
#define CMD_RAND_H

#include "crypto_command.h"

class cmd_rand : public crypto_command
{
    public:
        cmd_rand();
        virtual ~cmd_rand();

        virtual header id() const;
        virtual bool process(crypto_args & args, unpacker & req, packer & rsp);
};

#endif
