/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "crypto_command_router.h"

#include <cassert>

#include "Logger.h"
#include "types.h"

crypto_command_router::crypto_command_router(
        std::unique_ptr<crypto_args> && args) :
    args_(std::move(args))
{
}

crypto_command_router::~crypto_command_router()
{
    // Pineapples
}

bool crypto_command_router::route(
        const Buffer & req,
        Buffer & rsp)
{
    bool ok = false;
    unpacker u(req);
    packer p(rsp);
    header id;

    if (!u.unpack(id))
        LOG_WARN("Failed to get id from packet");
    else if (!route(id, *args_, u, p))
        LOG_WARN("Failed to process command {:04X}", underlying(id));
    else
    {
        ok = true;
        if (!u.done())
            LOG_WARN("Unprocessed data remaining in {:04X} request", underlying(id));
    }

    return ok;
}
