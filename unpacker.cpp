/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "unpacker.h"

#include <endian.h>

#include "Logger.h"

unpacker::unpacker(
        const Buffer & buff) :
    pos_(0),
    buff_(buff)
{
}

bool unpacker::done() const
{
    return buff_.size() == pos_;
}

bool unpacker::unpack_internal(uint8_t & out, size_t sizeof_value)
{
    bool ok = sufficient_data(sizeof_value);
    if (ok)
    {
        const uint8_t & data = *(buff_.data() + pos_);
        switch (sizeof_value)
        {
            case 1:
                out = data;
                break;

            case 2:
                reinterpret_cast<uint16_t &>(out) = be16toh(reinterpret_cast<const uint16_t &>(data));
                break;

            case 4:
                reinterpret_cast<uint32_t &>(out) = be32toh(reinterpret_cast<const uint32_t &>(data));
                break;

            case 8:
                reinterpret_cast<uint64_t &>(out) = be64toh(reinterpret_cast<const uint64_t &>(data));
                break;

            default:
                assert(!"Unsupported default input for default template for packet builder add");
                ok = false;
                break;
        }

        if (ok)
            pos_ += sizeof_value;
    }
    return ok;
}

bool unpacker::sufficient_data(size_t len) const
{
    bool ret = pos_ + len <= buff_.size();
    if (!ret)
        LOG_WARN("Attempted to read beyond length of packet");
    return ret;
}
