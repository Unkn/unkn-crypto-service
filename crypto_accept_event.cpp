/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "crypto_accept_event.h"

#include "Logger.h"
#include "crypto_client_event.h"

crypto_accept_event::crypto_accept_event(
        int fd,
        crypto_command_router & router) :
    epoller_accept_event(fd),
    router_(router)
{
}

crypto_accept_event::~crypto_accept_event()
{
    // Pineapples
}

std::unique_ptr<epoller_event> crypto_accept_event::new_client(
        int accept_fd,
        const struct sockaddr_in & info)
{
    crypto_client_event * e = nullptr;
    if (allowed_to_connect(info))
        e = new crypto_client_event(accept_fd, router_, info);
    else
        LOG_NOTICE("Rejected new client");
    return std::unique_ptr<epoller_event>(e);
}

bool crypto_accept_event::allowed_to_connect(
        const struct sockaddr_in & info __attribute__ ((unused))) const
{
    // TODO IP Filtering
    return true;
}
