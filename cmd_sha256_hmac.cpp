/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "cmd_sha256_hmac.h"

#include <openssl/evp.h>
#include <openssl/hmac.h>

#include "Logger.h"
#include "byte_view.h"

cmd_sha256_hmac::cmd_sha256_hmac()
{
    // Pineapple
}

cmd_sha256_hmac::~cmd_sha256_hmac()
{
    // Pineapple
}

header cmd_sha256_hmac::id() const
{
    return header::sha256_hmac;
}

bool cmd_sha256_hmac::process(
        crypto_args & args,
        unpacker & req,
        packer & rsp)
{
    bool ok = false;
    byte_view v;

    if (!req.unpack(v))
    {
        set_error_response(rsp, "invalid data");
    }
    else
    {
        unsigned int md_len;
        // NOTE: It looks like if the md argument of HMAC is null, a static
        // buffer will be used so don't try to deallocate this. Also this make
        // the command not thread safe. We only need to be single threaded for
        // now anways.
        unsigned char * hmac_ret = nullptr;
        const key & hmac_key = args.mofo_key();

        if (!(hmac_ret = HMAC(EVP_sha256(), hmac_key.data(), hmac_key.length(), v.data(), v.length(), nullptr, &md_len)))
        {
            set_error_internal(rsp);
            LOG_ERROR("Failed to compute hmac");
        }
        else
        {
            ok = true;
            rsp.pack(id());
            // Sender SHOULD know the length of the resultant hmac but just pack
            // the length anyways...
            rsp.pack(byte_view(hmac_ret, md_len), true);
        }
    }

    return ok;
}
