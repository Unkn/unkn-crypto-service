/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CRYPTO_COMMAND_ROUTER_FACTORY_H
#define CRYPTO_COMMAND_ROUTER_FACTORY_H

#include <memory>

#include "crypto_command_router.h"

// TODO long as fuck name :| reduce it
namespace crypto_command_router_factory
{
    std::unique_ptr<crypto_command_router> create(std::unique_ptr<crypto_args> && args);
}

#endif
