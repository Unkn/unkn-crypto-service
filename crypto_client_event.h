/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CRYPTO_CLIENT_EVENT_H
#define CRYPTO_CLIENT_EVENT_H

#include "epoller_event.h"
#include "crypto_command_router.h"
#include "LinuxSocket.h"

class crypto_client_event : public epoller_event
{
    public:
        crypto_client_event(int fd, crypto_command_router & router, const struct sockaddr_in & info);
        virtual ~crypto_client_event();

        virtual std::unique_ptr<epoller_job> process();

    private:
        crypto_command_router & router_;
        std::unique_ptr<LinuxSocket> socket_;
        // NOTE: There are no plans for asynchronous connections so a single in
        // and out buffer per client (or per thread) is fine to just reduce
        // allocations.
        Buffer in_;
        Buffer out_;

        bool get_length_packet();
        bool get_command_packet();
        bool read_packet();

        bool send_length_packet();
        bool send_packet();
};

#endif
