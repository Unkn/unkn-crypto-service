set(CMAKE_SYSTEM_NAME Linux)

set(BUILDROOT_TOOLCHAIN /home/amardikian/git/buildroot/output/host)

set(CMAKE_C_COMPILER        ${BUILDROOT_TOOLCHAIN}/bin/arm-linux-gcc)
set(CMAKE_CXX_COMPILER      ${BUILDROOT_TOOLCHAIN}/bin/arm-linux-g++)
set(CMAKE_FIND_ROOT_PATH    ${BUILDROOT_TOOLCHAIN}//arm-buildroot-linux-gnueabihf/sysroot)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
