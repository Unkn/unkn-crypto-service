/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CRYPTO_ARGS_H
#define CRYPTO_ARGS_H

#include <memory>

#include "key.h"

class crypto_args
{
    public:
        crypto_args() = delete;
        crypto_args(const crypto_args &) = delete;
        crypto_args(crypto_args &&) = delete;
        crypto_args(std::unique_ptr<key> && mofo_key);
        ~crypto_args();

        const key & mofo_key() const;

    private:
        /**
         * Top level key in the crypto service.
         *
         * The most memorable thing from my new guy speech at Futurex was jhan
         * and the motherfucker key 😂. This isn't the same as an MFK (master
         * file key) but I just like this spin on the name too much not to use
         * it somewhere lol.
         */
        std::unique_ptr<key> mofo_key_;
};

#endif
