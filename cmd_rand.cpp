/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "cmd_rand.h"

#include <openssl/rand.h>
#include <openssl/err.h>
#include <memory>

#include "Logger.h"
#include "header.h"
#include "byte_view.h"

cmd_rand::cmd_rand()
{
    // Pineapple
}

cmd_rand::~cmd_rand()
{
    // Pineapple
}

header cmd_rand::id() const
{
    return header::rand;
}

bool cmd_rand::process(
        crypto_args & args __attribute__ ((unused)),
        unpacker & req,
        packer & rsp)
{
    bool ok = false;
    uint32_t len;

    if (!req.unpack(len))
    {
        set_error_response(rsp, "length missing");
    }
    else
    {
        auto b = std::make_unique<unsigned char []>(len);
        if (!RAND_bytes(b.get(), len))
        {
            set_error_internal(rsp);
            LOG_ERROR("Failed to generate {:d} rand bytes: {}", len, ERR_error_string(ERR_get_error(), nullptr));
        }
        else
        {
            ok = true;
            rsp.pack(id());
            rsp.pack(byte_view(b.get(), len), true);
        }
    }

    return ok;
}
