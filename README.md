# Unkn Crypto Service

Just a service intended to be the sole thing running on a separate server for my
maplestory private server source to defer to for critical crypto operations that
aren't already compromised by maplestory itself... like the communication
establishment it does with the client...

See https://gitlab.com/Unkn/UnknMS for the private server source.

## What to do :|

Using a rpi3 as a test device at the moment independent of my system. To set up
a development pi the same as I have do somethin like:

```
git clone git@gitlab.com:Unkn/buildroot.git
cd buildroot
make raspberrypi3_defconfig
make
dd if=output/images/sdcard.img of=SD_CARD
```

You are responsible for securing the access to your hardware and all. That
buildroot configuration has ssh and no root password so I advise against using
it for anything other than testing as is.
