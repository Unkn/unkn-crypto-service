/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CRYPTO_COMMAND_ROUTER_H
#define CRYPTO_COMMAND_ROUTER_H

#include <memory>

#include "command_router.h"
#include "packer.h"
#include "unpacker.h"
#include "header.h"
#include "crypto_args.h"

// All for the sake of friendship D:
class crypto_command_router;
namespace crypto_command_router_factory
{
    std::unique_ptr<crypto_command_router> create(std::unique_ptr<crypto_args> && args);
}

using xcommand_router = command_router<header, crypto_args &, unpacker &, packer &>;

class crypto_command_router : protected xcommand_router
{
    friend std::unique_ptr<crypto_command_router> crypto_command_router_factory::create(std::unique_ptr<crypto_args> && args);

    public:
        crypto_command_router(std::unique_ptr<crypto_args> && args);
        virtual ~crypto_command_router();

        bool route(const Buffer & req, Buffer & rsp);

	protected:
		using xcommand_router::route;

    private:
        std::unique_ptr<crypto_args> args_;
};

#endif
