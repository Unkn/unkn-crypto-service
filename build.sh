#!/bin/sh -x

BIN=unkn-crypto-service
BUILD_DIR=pi3_build
# ONLY FOR DEV
IP=10.60.5.14

if [ ! -e $BUILD_DIR ]; then
    mkdir $BUILD_DIR
fi

cd $BUILD_DIR

if [ ! -e CMakeCache.txt ]; then
    if ! cmake -DCMAKE_TOOLCHAIN_FILE=../Toolchain-buildroot-rpi3.cmake -DCMAKE_BUILD_TYPE=Debug ..; then
        >&2 echo Failed to generate makefiles
    fi
fi

if ! make; then
    >&2 echo build error
    exit 1
fi

if ! nmap $IP | tr -s ' ' | grep '22/tcp open ssh'; then
    >&2 echo ssh unavailable for $ip
    exit 1
fi

ssh root@$IP mkdir /unkn
scp ./bin/${BIN} root@$IP:/unkn/
ssh root@$IP "killall -9 ${BIN} || /unkn/${BIN} < /dev/null > /tmp/${BIN}.out 2>&1 &"
