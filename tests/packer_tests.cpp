/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "packer_tests.h"

#include <endian.h>

TEST_F(packer_tests, pack_uint8_t)
{
    uint8_t a = 0xB3;
    p_.pack(a);
    ASSERT_EQ(1, b_.size());
    uint8_t d = *b_.data();
    ASSERT_EQ(a, d);
}

TEST_F(packer_tests, pack_uint16_t)
{
    uint16_t a = 0x1122;
    p_.pack(a);
    ASSERT_EQ(2, b_.size());
    uint16_t d = be16toh(reinterpret_cast<uint16_t &>(*b_.data()));
    ASSERT_EQ(a, d);
}

TEST_F(packer_tests, pack_uint32_t)
{
    uint32_t a = 0x11223344;
    p_.pack(a);
    ASSERT_EQ(4, b_.size());
    uint32_t d = be32toh(reinterpret_cast<uint32_t &>(*b_.data()));
    ASSERT_EQ(a, d);
}

TEST_F(packer_tests, pack_uint64_t)
{
    uint64_t a = 0x11223344AABBCCDD;
    p_.pack(a);
    ASSERT_EQ(8, b_.size());
    uint64_t d = be64toh(reinterpret_cast<uint64_t &>(*b_.data()));
    ASSERT_EQ(a, d);
}

TEST_F(packer_tests, pack_string_with_size)
{
    std::string s = "pineappbe";
    p_.pack(s, true);
    ASSERT_EQ(s.size() + sizeof(uint32_t), b_.size());
    uint32_t d = be32toh(reinterpret_cast<uint32_t &>(*b_.data()));
    ASSERT_EQ(s.size(), d);
    ASSERT_TRUE(memcmp(s.data(), b_.data() + sizeof(uint32_t), s.size()) == 0);
}

TEST_F(packer_tests, pack_string_without_size)
{
    std::string s = "pineapple";
    p_.pack(s, false);
    ASSERT_EQ(s.size(), b_.size());
    ASSERT_TRUE(memcmp(s.data(), b_.data(), s.size()) == 0);
}
