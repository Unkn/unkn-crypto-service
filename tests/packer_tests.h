/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "packer.h"

class packer_tests : public ::testing::Test
{
    public:
        packer_tests() :
            b_(),
            p_(b_)
        {
        }

    protected:
        Buffer b_;
        packer p_;
};
