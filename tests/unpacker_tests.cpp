/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "unpacker_tests.h"

#include <endian.h>

TEST_F(unpacker_tests, unpack_uint8_t)
{
    uint8_t a = 0xB3;
    b_.write(a);
    uint8_t d;
    ASSERT_TRUE(u_.unpack(d));
    ASSERT_EQ(a, d);
}

TEST_F(unpacker_tests, unpack_uint16_t)
{
    uint16_t a = 0x1122;
    uint16_t be = htobe16(a);
    b_.write(be);
    uint16_t d;
    ASSERT_TRUE(u_.unpack(d));
    ASSERT_EQ(a, d);
}

TEST_F(unpacker_tests, unpack_uint32_t)
{
    uint32_t a = 0x11223344;
    uint32_t be = htobe32(a);
    b_.write(be);
    uint32_t d;
    ASSERT_TRUE(u_.unpack(d));
    ASSERT_EQ(a, d);
}

TEST_F(unpacker_tests, unpack_uint64_t)
{
    uint64_t a = 0x11223344AABBCCDD;
    uint64_t be = htobe64(a);
    b_.write(be);
    uint64_t d;
    ASSERT_TRUE(u_.unpack(d));
    ASSERT_EQ(a, d);
}

TEST_F(unpacker_tests, unpack_string_with_size)
{
    std::string s = "pineapple";
    uint32_t a = s.size();
    uint32_t be = htobe32(a);
    b_.write(be);
    b_.write(s.data(), a);
    std::string_view v;
    ASSERT_TRUE(u_.unpack(v));
    ASSERT_EQ(s, v);
}

TEST_F(unpacker_tests, unpack_string_without_size)
{
    std::string s = "pineapple";
    b_.write(s.data(), s.size());
    std::string_view v;
    ASSERT_TRUE(u_.unpack(v, s.size()));
    ASSERT_EQ(s, v);
}
