add_executable(unkn-crypto-tests
    main.cpp
    packer_tests.cpp
    unpacker_tests.cpp
    )
target_link_libraries(unkn-crypto-tests
    gtest
    gmock
    unkn-crypto
    )
set_property(TARGET unkn-crypto-tests PROPERTY CXX_STANDARD 17)
