/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <cassert>
#include <endian.h>

#include "packer.h"

packer::packer(
        Buffer & buff) :
    buff_(buff)
{
}

void packer::pack_internal(const uint8_t & value, size_t sizeof_value)
{
    switch (sizeof_value)
    {
        case 1:
            buff_.write(value);
            break;

        case 2:
            buff_.write(htobe16(reinterpret_cast<const uint16_t &>(value)));
            break;

        case 4:
            buff_.write(htobe32(reinterpret_cast<const uint32_t &>(value)));
            break;

        case 8:
            buff_.write(htobe64(reinterpret_cast<const uint64_t &>(value)));
            break;

        default:
            assert(!"Unsupported default input for default template for packet builder add");
            break;
    }
}
