/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef BYTE_VIEW_H
#define BYTE_VIEW_H

#include <cstdint>
#include <string_view>

using byte_view = std::basic_string_view<uint8_t>;

#endif
