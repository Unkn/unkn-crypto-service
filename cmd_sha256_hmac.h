/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CMD_SHA256_HMAC_H
#define CMD_SHA256_HMAC_H

#include "crypto_command.h"

#include <memory>

#include "key.h"

class cmd_sha256_hmac : public crypto_command
{
    public:
        cmd_sha256_hmac();
        virtual ~cmd_sha256_hmac();

        virtual header id() const;
        virtual bool process(crypto_args & args, unpacker & req, packer & rsp);
};

#endif
