/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef TYPES_H
#define TYPES_H

// TODO Add underlying methods for lvalues and rvalues instead of stricly lvalues
template<typename T>
static inline auto & underlying(T & t)
{
    // TODO If this doesnt work as expected then go back to
    //return *reinterpret_cast<typename std::underlying_type<T>::type *>(&t);
    return reinterpret_cast<typename std::underlying_type<T>::type &>(t);
}

template<typename T>
static inline const auto & underlying(const T & t)
{
    return reinterpret_cast<typename std::underlying_type<T>::type &>(t);
}

#endif
