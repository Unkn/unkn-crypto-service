/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "crypto_server.h"

#include <openssl/err.h>

#include "Logger.h"
#include "StdOutLogMethod.h"
#include "network_utils.h"
#include "epoller_factory.h"
#include "epoller_sigint_event.h"
#include "crypto_accept_event.h"
#include "crypto_command_router_factory.h"

#define CRYPTO_SERVER_PORT  9999
#define CONNECT_BACKLOG     10

crypto_server::crypto_server()
{
    // Pineapples
}

crypto_server::~crypto_server()
{
    // Pineapples
}

int crypto_server::run()
{
    USE_STDOUT_LOG_METHOD;

    LOG_NOTICE("Initializing crypto server");
    ERR_load_crypto_strings();

    bool ok = false;
    std::unique_ptr<epoller> e;
    std::unique_ptr<crypto_command_router> r;

    if (!(e = epoller_factory::create()))
        LOG_CRIT("Failed to initialize epoller");
    else if (!register_sigint_event(*e))
        LOG_CRIT("Failed to registering sigint event");
    else if (!(r = initialize_router()))
        LOG_CRIT("Failed to initialize command router");
    else if (!register_listen_event(*e, *r))
        LOG_CRIT("Failed to initialize listener event");
    else
        ok = true;

    if (ok)
    {
        LOG_NOTICE("Crypto server ready for processing");
        while (e->wait_and_process());
    }
    else
    {
        LOG_NOTICE("Failed to initialize crypto server");
    }

    LOG_NOTICE("Shutting down crypto server");
    ERR_free_strings();

    return ok ? 0 : 1;
}

std::unique_ptr<crypto_args> crypto_server::initialize_crypto_args()
{
    auto k = load_key();
    if (!k)
    {
        LOG_CRIT("Failed to load mofo key");
        return nullptr;
    }
    return std::make_unique<crypto_args>(std::move(k));
}

std::unique_ptr<crypto_command_router> crypto_server::initialize_router()
{
    auto a = initialize_crypto_args();
    if (!a)
    {
        LOG_CRIT("Failed to initialize crypto args");
        return nullptr;
    }
    return crypto_command_router_factory::create(std::move(a));
}

bool crypto_server::register_sigint_event(
        epoller & e)
{
    bool ok = true;
    try
    {
        e.add_event(std::make_unique<epoller_sigint_event>());
    }
    catch (const std::exception & ex)
    {
        ok = false;
        LOG_CRIT("Error registering sigint event: {}", ex.what());
    }
    return ok;
}

bool crypto_server::register_listen_event(
        epoller & e,
        crypto_command_router & r)
{
    bool ok = true;
    int listen_fd = network_utils::listen_socket_any(CRYPTO_SERVER_PORT, CONNECT_BACKLOG);
    if (listen_fd < 0)
        ok = false;
    else
        e.add_event(std::make_unique<crypto_accept_event>(listen_fd, r));
    return ok;
}
