/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CRYPTO_ACCEPT_EVENT_H
#define CRYPTO_ACCEPT_EVENT_H

#include "epoller_accept_event.h"
#include "crypto_command_router.h"

class crypto_accept_event : public epoller_accept_event
{
    public:
        crypto_accept_event(int fd, crypto_command_router & router);
        virtual ~crypto_accept_event();

        virtual std::unique_ptr<epoller_event> new_client(int accept_fd, const struct sockaddr_in & info);

    private:
        crypto_command_router & router_;

        bool allowed_to_connect(const struct sockaddr_in & info) const;
};

#endif
