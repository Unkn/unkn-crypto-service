/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef HEADER_H
#define HEADER_H

#include <cstdint>

/**
 * Collection of command headers
 *
 * @note Keep these ordered by the header value instead of the names in order to
 * prevent the reuse of existing header values.
 *
 * @note 2 bytes is more than enough to account for all of the commands that
 * will ever be added
 */
enum class header : uint16_t
{
    error           = 0x0000,
    rand            = 0x0001,
    sha256_hmac     = 0x0002,
    aes256_gcm      = 0x0003,
};

#endif
