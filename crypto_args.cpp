/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "crypto_args.h"

crypto_args::crypto_args(
        std::unique_ptr<key> && mofo_key) :
    mofo_key_(std::move(mofo_key))
{
}

crypto_args::~crypto_args()
{
}

const key & crypto_args::mofo_key() const
{
    return *mofo_key_;
}
