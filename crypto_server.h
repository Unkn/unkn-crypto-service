/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CRYPTO_SERVER_H
#define CRYPTO_SERVER_H

#include <memory>

#include "Program.h"
#include "crypto_args.h"
#include "crypto_command_router.h"
#include "epoller.h"

class crypto_server : public Program
{
    public:
        crypto_server();
        virtual ~crypto_server();

    protected:
        virtual int run();

    private:
        std::unique_ptr<crypto_args> initialize_crypto_args();
        std::unique_ptr<crypto_command_router> initialize_router();
        bool register_sigint_event(epoller & e);
        bool register_listen_event(epoller & e, crypto_command_router & r);
};

#endif
