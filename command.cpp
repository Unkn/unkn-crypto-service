/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "command.h"

void command::set_error_response(packer & rsp, std::string_view error) const
{
    rsp.pack(header::error);
    rsp.pack(error);
}

void command::set_error_internal(packer & rsp) const
{
    set_error_response(rsp, "internal error");
}
