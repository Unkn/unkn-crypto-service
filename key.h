/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef KEY_H
#define KEY_H

#include <cstdint>

enum class key_type
{
    aes256
};

class key
{
    public:
        key(key_type type);
        virtual ~key();

        uint8_t * data();
        const uint8_t * data() const;
        int length() const;
        key_type type() const;

    private:
        key_type type_;
        int len_;
        uint8_t * data_;
};

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Temporary
//******************************************************************************
//******************************************************************************
//******************************************************************************

#include <memory>

// Because right now there will only be 1 key :|
std::unique_ptr<key> load_key();

#endif
