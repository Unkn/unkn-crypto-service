/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef PACKER_H
#define PACKER_H

#include <string_view>

#include "buffer.hpp"

class packer
{
    public:
        packer(Buffer & buff);

        template<typename T>
        void pack(const T & data)
        {
            pack_internal(reinterpret_cast<const uint8_t &>(data), sizeof(T));
        }

        template<typename T>
        void pack(const std::basic_string_view<T> & data, bool add_len)
        {
            uint32_t len = data.size();
            if (add_len)
                pack(len);
            buff_.write(data.data(), len);
        }

        template<typename T>
        void pack(const std::basic_string<T> & data, bool add_len)
        {
            pack(std::basic_string_view<T>(data), add_len);
        }

    private:
        void pack_internal(const uint8_t & data, size_t sizeof_value);

        Buffer & buff_;
};

#endif
