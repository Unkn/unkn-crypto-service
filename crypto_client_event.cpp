/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "crypto_client_event.h"

#include <endian.h>

#include "Logger.h"
#include "epoller_job_del.h"
#include "unpacker.h"

crypto_client_event::crypto_client_event(
        int fd,
        crypto_command_router & router,
        const struct sockaddr_in & info __attribute__ ((unused))) : // TODO Store connection info
    // TODO Need better coexistence between epoller_event and LinuxSocket. The
    // LinuxSocket will handle closing the fd.
    epoller_event(fd, false),
    router_(router),
    socket_(std::make_unique<LinuxSocket>(fd))
{
}

crypto_client_event::~crypto_client_event()
{
    // Pineapples
}

std::unique_ptr<epoller_job> crypto_client_event::process()
{
    bool ok = false;

    if (!read_packet())
        LOG_ERROR("Failed to read packet.");
    else if (!router_.route(in_, out_))
        LOG_ERROR("Failed to process message.");
    else if (!send_packet())
        LOG_ERROR("Failed to send response to client");
    else
        ok = true;

    return ok ? nullptr : std::make_unique<epoller_job_del>();
}

bool crypto_client_event::get_length_packet()
{
    bool ok = false;
    uint32_t len;
    unpacker u(in_);
    in_.resize(sizeof(len));

    if (!socket_->recv(in_))
        LOG_ERROR("Failed to read length header");
    else if (!u.unpack(len))
        LOG_ERROR("Failed to unpack length header");
    else
    {
        ok = true;
        in_.resize(len);
    }

    return ok;
}

bool crypto_client_event::get_command_packet()
{
    bool ok = false;
    uint32_t len = in_.size();
    in_.resize(len);

    if (!socket_->recv(in_))
        LOG_ERROR("Failed to read length header");
    else if (in_.size() != len)
        LOG_ERROR("Read incomplete command packet");
    else
        ok = true;

    return ok;
}

bool crypto_client_event::read_packet()
{
    return get_length_packet() && get_command_packet();
}

bool crypto_client_event::send_length_packet()
{
    uint32_t len = htobe32(out_.size());
    return socket_->send(reinterpret_cast<uint8_t *>(&len), sizeof(len));
}

bool crypto_client_event::send_packet()
{
    return send_length_packet() && socket_->send(out_);
}
