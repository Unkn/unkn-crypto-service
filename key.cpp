/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "key.h"

#include <cassert>

#include "zeroize.h"

key::key(key_type type)
{
    type_ = type;
    switch (type_)
    {
        case key_type::aes256:
            len_ = 32;
            break;

        default:
            assert(!"invalid key type");
            break;
    }
    data_ = new uint8_t[len_];
    zeroize(data_, len_);
}

key::~key()
{
    zeroize(data_, len_);
    delete [] data_;
}

uint8_t * key::data()
{
    return data_;
}

const uint8_t * key::data() const
{
    return data_;
}

int key::length() const
{
    return len_;
}

key_type key::type() const
{
    return type_;
};

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Temporary
//******************************************************************************
//******************************************************************************
//******************************************************************************

#include <cstdio>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>


static off_t file_size(const char * file)
{
    struct stat s;
    if (stat(file, &s) < 0)
    {
        perror("stat");
        return -1;
    }
    return s.st_size;
}

std::unique_ptr<key> load_key()
{
    // Too bad no sram on hand to play with... gonna have to be insecure as
    // fuck :P
    //
    // For testing just 'sudo dd if=/dev/urandom of=/fuk_me bs=32 count=1' to
    // initialize a bullshit key. Should use RAND_bytes to generate the key
    // though for real.
    const char * KEY_LOC = "/fuk_me";
    auto k = std::make_unique<key>(key_type::aes256);

    // Expect a sha256 key... just check the length and assume its what we want
    // because this is already fucked ;)
	if (file_size(KEY_LOC) != k->length())
    {
        fprintf(stderr, "Invalid key length\n");
        return nullptr;
    }

    int fd = open(KEY_LOC, 0);
    if (fd < 0)
    {
        perror("open");
        return nullptr;
    }

    // Assume 1 read is sufficient
    ssize_t r = read(fd, k->data(), k->length());
    if (r < 0)
    {
        perror("read");
        k.reset();
    }
    else if (r != k->length())
    {
        fprintf(stderr, "Invalid read length\n");
        k.reset();
    }

    close(fd);

    return k;
}
