/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "crypto_command_router_factory.h"

#include "cmd_rand.h"
#include "cmd_sha256_hmac.h"

std::unique_ptr<crypto_command_router> crypto_command_router_factory::create(
        std::unique_ptr<crypto_args> && args)
{
    auto r = std::make_unique<crypto_command_router>(std::move(args));

    r->register_command(std::move(std::make_unique<cmd_rand>()));
    r->register_command(std::move(std::make_unique<cmd_sha256_hmac>()));

    return r;
}
