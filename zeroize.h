/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef ZEROIZE_H
#define ZEROIZE_H

#include <cstring>

// Knew the memset can get optimized out by the compiler if it was the last use
// of the buffer but forgot how to prevent it from getting optimized out.
//
// Google of 'volatile memset optimization' yields:
//
//     http://www.daemonology.net/blog/2014-09-04-how-to-zero-a-buffer.html
//
static void * (* const volatile memset_ptr)(void *, int, size_t) = memset;
static inline void zeroize(void * p, size_t len)
{
    (memset_ptr)(p, 0, len);
}

#endif
