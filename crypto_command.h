/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CRYPTO_COMMAND_H
#define CRYPTO_COMMAND_H

#include <string_view>

#include "command.h"
#include "packer.h"
#include "unpacker.h"
#include "header.h"
#include "crypto_args.h"

class crypto_command : public command<header, crypto_args &, unpacker &, packer &>
{
    public:
        virtual header id() const = 0;
        virtual bool process(crypto_args & args, unpacker & req, packer & rsp) = 0;

    protected:
        void set_error_response(packer & rsp, std::string_view error) const;
        void set_error_internal(packer & rsp) const;
};

#endif
