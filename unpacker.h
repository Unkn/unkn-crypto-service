/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef UNPACKER_H
#define UNPACKER_H

#include <cassert>
#include <string_view>

#include "buffer.hpp"

class unpacker
{
    public:
        unpacker(const Buffer & buff);

        template<typename T>
        bool unpack(T & data)
        {
            return unpack_internal(reinterpret_cast<uint8_t &>(data), sizeof(T));
        }

        template<typename T>
        bool unpack(std::basic_string_view<T> & data)
        {
            uint32_t len;
            return unpack(len) && unpack(data, len);
        }

        template<typename T>
        bool unpack(std::basic_string_view<T> & data, uint32_t len)
        {
            // TODO Only want to deal with single width characters and byte
            // arrays at the moment
            static_assert(sizeof(T) == 1, "Only single width string_view's can be packed");

            bool ok = sufficient_data(len);
            if (ok)
            {
                data = std::basic_string_view<T>(reinterpret_cast<const T *>(buff_.data() + pos_), len);
                pos_ += len;
            }
            return ok;
        }

        bool done() const;

    private:
        bool unpack_internal(uint8_t & value, size_t sizeof_value);
        bool sufficient_data(size_t len) const;

        size_t pos_;
        const Buffer & buff_;
};

#endif
